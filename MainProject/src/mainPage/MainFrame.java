package mainPage;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

import javax.swing.*;

import common.Blocks;
import common.Card;
import common.Players;
import common.Properties;
import common.PublicData;
import gamePanels.MainPanel;
import gamePanels.PlayerPanel;
import gamePanels.PropertyPanel;

/**
 * Game main frame 
 * @author Cheng Chen & Ruibin Chen
 *
 */
@SuppressWarnings("serial")
public class MainFrame extends JFrame {	
	private int numOfPly, numOfPro, numOfHouse, numOfHotel;
	private MainPanel mainPanel;
	private PropertyPanel propertyPanel;
	private PlayerPanel playerPanel;
	private ControlPanel controlPanel;
	private Players players[]; 
	private Properties property[];
	private Blocks blocks;
	private Card cards;
	private Dimension frameSize = new Dimension(1900, 1040);
	
	/**
	 * Create game main frame 
	 * @param nPly number of player
	 */
	public MainFrame(int nPly) {
		numOfPly = nPly;
		numOfPro = PublicData.numProperty;
		numOfHouse = PublicData.numHouse;
		numOfHotel = PublicData.numHotel;
		
		setTitle("Monopoly");
		setSize(frameSize);
		setLayout(null);
		
		// initiate blocks 
		blocks = new Blocks();
		
		// initiate chance and fate cards
		cards = new Card();
		
		// initiate all players (players must initiated before panels)
		players = new Players[numOfPly];
		for(int i=0; i<numOfPly; i++) {
			players[i] = new Players(PublicData.plyName[i]);
		}
		
		// initiate all properties 
		property = new Properties[numOfPro];
		for(int i=0; i<numOfPro; i++) {
			property[i] = new Properties((String)PublicData.proInfo[i][PublicData.proIndex.NAME.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.PRICE.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.RENT.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE1.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE2.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE3.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOUSE4.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HOTEL.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.HCOST.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.BLOCK.ordinal()],
					   (String)PublicData.proInfo[i][PublicData.proIndex.COLOR.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.NHOUSE.ordinal()],
					   (int)PublicData.proInfo[i][PublicData.proIndex.NHOTEL.ordinal()],
					   (String)PublicData.proInfo[i][PublicData.proIndex.OWNER.ordinal()]);
		}
		
		// initiate all panels
		mainPanel = new MainPanel(10, 10, 985, 985, numOfPly);
		propertyPanel = new PropertyPanel(1015, 10, 400, 700);
		playerPanel = new PlayerPanel(1425, 10, 445, 700);
		controlPanel = new ControlPanel(1015, 720, 855, 275);
		
		// add panels
		add(mainPanel);
		add(propertyPanel);
		add(playerPanel);
		add(controlPanel);
	}
	
	/**
	 * Creates Control Panel, manage all game operations 
	 * @author Cheng Chen
	 *
	 */
	public class ControlPanel extends JPanel {	
		private JLabel currentBid, currentPly, remainHouse, remainHotel, remainPro, dice1, dice2;
		private JComboBox<String> proSelection;
		private JComboBox<String> groupSelection;
		private JComboBox<String> houseSelection;
		private JComboBox<String> bidSelection;
		private JButton btnGo, btnView, btnBid, btnEndBid, btnSearch, btnSellPro, btnBuyHouse, btnSellHouse;
		private ImageIcon imgDice1, imgDice2;
		private int playerNo, bidPro, bidValue;
		private boolean endTurn = false; 
		private boolean isBid = false;
		
		/**
		 * Setup ControlPanel 
		 * @param x position in main frame 
		 * @param y position in main frame
		 * @param width panel width
		 * @param height panel height
		 */
		public ControlPanel(int x, int y, int width, int height) {
			setBounds(x, y, width, height);
			setBorder(BorderFactory.createLineBorder(Color.black, 3));
			setBackground(new Color(169,209,142));
			setLayout(null);
			
			initComponents();
			
			add(currentBid);
			add(currentPly);
			add(remainHouse);
			add(remainHotel);
			add(remainPro);
			add(dice1);
			add(dice2);
			add(proSelection);
			add(groupSelection);
			add(houseSelection);
			add(bidSelection);
			add(btnSellPro);
			add(btnSellHouse);
			add(btnSearch);
			add(btnBuyHouse);
			add(btnGo);
			add(btnView);
			add(btnBid);
			add(btnEndBid);
		}
		
		/**
		 * Initiate all components 
		 */
		private void initComponents() {	
			dice1 = new JLabel();
			dice2 = new JLabel();
			currentBid = new JLabel("Current Bid: 50��");
			currentPly = new JLabel("Current Player: Player 1");
			remainHouse = new JLabel("Rest Number of House: 32");
			remainHotel = new JLabel("Rest Number of Hotel: 12");
			remainPro = new JLabel("Rest Number of Free Property: 28");
			proSelection = new JComboBox<String>();
			groupSelection = new JComboBox<String>();
			houseSelection = new JComboBox<String>();
			bidSelection = new JComboBox<String>();
			btnGo = new JButton("GO");
			btnView = new JButton("View");
			btnBid = new JButton("Bid");
			btnEndBid = new JButton("End Bid");
			btnSearch = new JButton("Find Property");
			btnSellPro = new JButton("Sell Property");
			btnBuyHouse = new JButton("Buy House");
			btnSellHouse = new JButton("Sell House");
			
			currentPly.setBounds(50, 10, 250, 50);
			remainHouse.setBounds(50, 50, 200, 50);
			remainHotel.setBounds(50, 80, 200, 50);
			remainPro.setBounds(50, 110, 250, 50);
			currentBid.setBounds(50, 200, 180, 50);
			bidSelection.setBounds(50, 170, 120, 30);
			proSelection.setBounds(480, 30, 210, 30);
			groupSelection.setBounds(480, 70, 210, 30);
			houseSelection.setBounds(480, 110, 210, 30);
			dice1.setBounds(650, 170, 80, 80);
			dice2.setBounds(740, 170, 80, 80);
			
			btnSellPro.setBounds(700, 30, 120, 30);
			btnBuyHouse.setBounds(700, 70, 120, 30);
			btnSellHouse.setBounds(700, 110, 120, 30);
			btnSearch.setBounds(350, 30, 120, 30);
			btnGo.setBounds(540, 190, 100, 60);
			btnView.setBounds(180, 170, 80, 30);
			btnBid.setBounds(270, 170, 80, 30);
			btnEndBid.setBounds(230, 210, 120, 30);
			
			currentBid.setFont(new Font("currentBid", Font.ITALIC, 15));
			
			/* Go button activities*/
			btnGo.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(!isBid) {
						// Dice
						if(endTurn) {
							playerNo++;
							if(playerNo == numOfPly)
								playerNo = 0;
							
							endTurn = false;
							btnGo.setText("GO");
							plySwitch(playerNo);
						} else {
							endTurn = true; 
							btnGo.setText("End Turn");
							plyMove(dice(), playerNo);
						}	
					}		
				}
			});
			
			/* Buy House button activities*/
			btnBuyHouse.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(groupSelection.getSelectedItem() != null  && !isBid) {
						buyHouse(playerNo, (String)groupSelection.getSelectedItem());
					}
				}
			});
			
			/* Search Property button activities*/
			btnSearch.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(!isBid)
						updatePanelInfo("property", propertyPanel.getSelectedPro());
				}
			});
			
			/* Sell Property button activities*/
			btnSellPro.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(proSelection.getSelectedItem() != null && !isBid) {
						sellProperty(playerNo, (String)proSelection.getSelectedItem());
					}
				}
			});
			
			/* Sell House button activities*/
			btnSellHouse.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(houseSelection.getSelectedItem() != null && !isBid) {
						sellHouse(playerNo, (String)houseSelection.getSelectedItem());
					}
				}
			});
			
			/* Bid view button activities*/
			btnView.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(isBid && bidSelection.getSelectedItem()!=null) {
						// view selected player information 
						updatePanelInfo("player", blocks.findPlyNo((String)bidSelection.getSelectedItem()));
					}
				}
			});
			
			/* Bid button activities*/
			btnBid.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(isBid && bidSelection.getSelectedItem()!=null) {
						int plyNo = blocks.findPlyNo((String)bidSelection.getSelectedItem());
						// add current bid value 
						if(bidValue+10 <= players[plyNo].getCash()) {  // if the player has enough cash to make bid
							bidValue += 10;
							currentBid.setText("Current Bid: " + String.valueOf(bidValue) + "��");
						}
					}
				}   
			});
			
			btnEndBid.addActionListener(new ActionListener()
			{
				public void actionPerformed(ActionEvent e)
				{
					if(isBid && bidSelection.getSelectedItem()!=null) {
						// end bid action  
						isBid = false;
						numOfPro--;
						int plyNo = blocks.findPlyNo((String)bidSelection.getSelectedItem());
						players[plyNo].reduceCash(bidValue);
						players[plyNo].addPro(property[bidPro].getName(), bidPro);
						property[bidPro].setOwner(players[plyNo].getName());
						updatePanelInfo("player", plyNo);
						updatePanelInfo("property", bidPro);
						updatePanelInfo("control", playerNo);  // only update remaining property info 
						
						JOptionPane.showConfirmDialog(null, "The highest bidder is: " + players[plyNo].getName()
								                      + "\nBid price: " + bidValue + "��" + "\nBid property: " +
								                      property[bidPro].getName(), "Bid End",
								                      JOptionPane.YES_NO_OPTION);
						
						updatePanelInfo("player", playerNo);  // player panel back to current player
						updateBid(isBid);  // turn off bid area
					}
				}
			});
			
			// initial info of control panel 
			updateBid(isBid);
			setPubInfo(players[0].getName(), PublicData.numHouse, PublicData.numHotel, PublicData.numProperty);
			showDice(1, 1);			
		}
		
		/**
		 * Set public information of Panel 
		 * @param ply name of player
		 * @param nHouse total house remaining 
		 * @param nHotel total hotel remaining
		 * @param nPro total free properties remaining 
		 */
		public void setPubInfo(String ply, int nHouse, int nHotel, int nPro) {
			// load owned color groups of current player 
			proSelection.removeAllItems();
			groupSelection.removeAllItems();
			houseSelection.removeAllItems();
			String ColorGroup[] = players[playerNo].getColorGroup();
			int Property[] = players[playerNo].getProperty();
			for(int i=0; i<10; i++) {
				if (ColorGroup[i] != null) {
					groupSelection.addItem(ColorGroup[i]);
					houseSelection.addItem(ColorGroup[i]);
				}
			}
			
			for(int i=0; i<PublicData.numProperty; i++) {
				if(Property[i] >= 0) {
					proSelection.addItem((String)PublicData.proInfo[i][PublicData.proIndex.NAME.ordinal()]);
				}
			}
			
			currentPly.setText("Current Player: " + ply);
			currentPly.setFont(new Font("currentPly", Font.ITALIC, 20));
			currentPly.setForeground(Color.red);
			
			remainHouse.setText("House Remaining: " + String.valueOf(nHouse));
			remainHouse.setFont(new Font("remainHouse", Font.ITALIC, 15));
			
			remainHotel.setText("Hotel Remaining: " + String.valueOf(nHotel));
			remainHotel.setFont(new Font("remainHotel", Font.ITALIC, 15));
			
			remainPro.setText("Property Remaining: " + String.valueOf(nPro));
			remainPro.setFont(new Font("currentPly", Font.ITALIC, 15));
		}
		
		/**
		 * Show dice value 
		 * @param dicePoint1 dice 1
		 * @param dicePoint2 dice 2
		 */
		public void showDice(int dicePoint1, int dicePoint2) {
			imgDice1 = new ImageIcon(this.getClass().getClassLoader().getResource("images/Dice/" + String.valueOf(dicePoint1) + ".png"));
			imgDice1.setImage(imgDice1.getImage().getScaledInstance(dice1.getWidth(), 
					dice1.getHeight(), Image.SCALE_DEFAULT ));
			
			imgDice2 = new ImageIcon(this.getClass().getClassLoader().getResource("images/Dice/" + String.valueOf(dicePoint2) + ".png"));
			imgDice2.setImage(imgDice2.getImage().getScaledInstance(dice2.getWidth(), 
					dice2.getHeight(), Image.SCALE_DEFAULT ));
			
			dice1.setIcon(imgDice1);
			dice2.setIcon(imgDice2);
		}
		
		/**
		 * Return dice value  
		 * @return sum of 2 dice results 
		 */
		public int dice() {
			int result1 = new Random().nextInt(6) + 1;
			int result2 = new Random().nextInt(6) + 1;
			showDice(result1, result2);
			//System.out.println("result1: " + result1);
			//System.out.println("result2: " + result2);
			return result1 + result2;
			//return 1;
		}
		
		/**
		 * Move the player 
		 * @param dicePoint how many blocks the player should push 
		 * @param plyNo number of current player
		 */
		public void plyMove(int dicePoint, int plyNo) {
			int blockNo = players[plyNo].blockCalculation(dicePoint);
			
			mainPanel.plyCalculation(blockNo, plyNo);
			blockActivity(blockNo, plyNo);
		}
		
		
		/**
		 * Switch to next player 
		 * @param plyNo number of current player
		 */
		public void plySwitch(int plyNo) {
			// update player info panel for next player 
			updatePanelInfo("player", plyNo);
			
			// update current player name on control panel 
			updatePanelInfo("control", plyNo);
			
			if(players[plyNo].getState()) {
				inJail(plyNo);
			}
		}
		
		/* --------------------------------Main Logic Functions--------------------------------*/
		
		/**
		 * Execute activities on specific block
		 * @param block block No. of current player
		 * @param plyNo number of current player
		 */
		public void blockActivity(int blockNo, int plyNo) {
			switch(blockNo) {
			case 0:
			case 10:
			case 20:
				// Nothing happened
				break;
			case 4:
			case 13:
			case 28:
				// draw chance card
				drawChanceCard(cards.getChanceCard(),plyNo, blockNo);
				break;
			case 8:
			case 24:
			case 33:
				// draw fate card
				drawFateCard(cards.getFateCard(), plyNo);
				break;
			case 15:
			case 35:
				// pay tax
				payTax(plyNo);
				break;
			case 30:
				// go to jail
				inJail(plyNo);
				break;
			default:
				// property activity
				proActivity(blockNo, plyNo);
				break;
			}
		}
		
		/**
		 * Start bid activity when a player doesn't buy a free property
		 * @param proNo  property number 
		 * @param plyNo  player number 
		 */
		public void bidActivity(int proNo, int plyNo) {
			JOptionPane.showConfirmDialog(null, "Start Bid on Property: " 
										  + property[proNo].getName(), "Bid",
										  JOptionPane.YES_NO_OPTION);
			
			isBid = true;
			updateBid(isBid);  // turn on bid area 
			bidPro = proNo;
			bidSelection.removeAllItems();
			/* load bid selection list*/
			for(int i=0; i<numOfPly; i++) {
				if(PublicData.plyName[i] != players[plyNo].getName()) {
					bidSelection.addItem(PublicData.plyName[i]); 
				}
			}
		}
		
		/**
		 * Execute activities on a property  
		 * @param blockNo block number
		 * @param plyNo player number 
		 */
		public void proActivity(int blockNo, int plyNo) {
			int proNo = blocks.findProNo(blockNo);  // find corresponding property objective
			//System.out.println("proNo" + proNo);
			
			// update property panel info 
			updatePanelInfo("property", proNo);
			
			/* If on a free property*/
			if(property[proNo].getOwner() == "Null") {
				int price = blocks.proPurchase(property[proNo].getPrice(), property[proNo].getName());
				if(price == 0) {  
					// bank sells the property (Bid)
					bidActivity(proNo, plyNo);
				} else if(players[plyNo].getCash() > price) {  // if player says YES and able to pay
					numOfPro--;
					//Player update
					players[plyNo].reduceCash(price);
					players[plyNo].addPro(property[proNo].getName(), proNo);
					players[plyNo].addColor(proNo);
					
					//Property update
					property[proNo].setOwner(players[plyNo].getName());	
				} else {
					blocks.canNotPurchase();
				}
				
		    /* If on a owned property of other players*/
			} else if (property[proNo].getOwner() != players[plyNo].getName()) {
				int rent = property[proNo].getRent();
				if(blocks.findProType(blockNo) == "energy") {  // if on energy company
					rent = 4*blocks.payRent(rent, property[proNo].getOwner()); 
				} else {
					rent = blocks.payRent(rent, property[proNo].getOwner());
				}
				
				// player pay rent to property owner 
				players[plyNo].reduceCash(rent);  
				players[blocks.findPlyNo(property[proNo].getOwner())].addCash(rent);
			}
			
			// update player panel info
			updatePanelInfo("player", plyNo);
			
			// update property panel info 
			updatePanelInfo("property", proNo);
			
			// update control panel info 
			updatePanelInfo("control", plyNo);
		}
		
		/**
		 * Pay tax 
		 * @param plyNo player number 
		 */
		public void payTax(int plyNo) {
			players[plyNo].reduceCash(PublicData.tax);
			// update player panel info
			updatePanelInfo("player", plyNo);
			JOptionPane.showMessageDialog(null, "You need pay tax 200�� !", "Pay tax",
	                JOptionPane.ERROR_MESSAGE);
		}
		
		/**
		 * Sell the property between players
		 * @param plyNo  current player number 
		 * @param proName  the property on sail 
		 */
		public void sellProperty(int plyNo, String proName) {
			int proNo = 0;
			/* find corresponding property No.*/
			for(int i=0; i<PublicData.numProperty; i++) {
				if(proName == (String)PublicData.proInfo[i][PublicData.proIndex.NAME.ordinal()]) {
					proNo = i;
					break;
				}
			}
			
			/* if buildings on the property, sell buildings first*/
			if(property[proNo].getHouse() > 0 || property[proNo].getHotel() > 0) {
				JOptionPane.showMessageDialog(null, "You have to sell all buildings"
											  + "on this property before you sell it", 
											  "Error", JOptionPane.ERROR_MESSAGE);
			} else {
				/* sell the property to other player*/
				Object[] plyOption = new Object[numOfPly-1];
				int counter = 0;
				for(int i=0; i<numOfPly; i++) {
					if(PublicData.plyName[i] != players[plyNo].getName()) {
						plyOption[counter] = PublicData.plyName[i];
						counter++;
					}
				}
				
				Object targetPly = JOptionPane.showInputDialog(null, 
						"Which player you want sell this property to?", "Selling property",
						JOptionPane.INFORMATION_MESSAGE, null, 
						plyOption, plyOption[0]);
				
				if(targetPly != null) {
					int targetPlyNo = blocks.findPlyNo((String)targetPly);
					
					String inputPrice = JOptionPane.showInputDialog("Please input your price:");
					int price = Integer.parseInt(inputPrice);
					// if the target player is able to pay the price
					if(price >= 0 && price <= players[targetPlyNo].getCash()) {
						players[plyNo].addCash(price);
						players[plyNo].reducePro(proNo);
						players[targetPlyNo].reduceCash(price);
						players[targetPlyNo].addPro(proName, proNo);
						property[proNo].setOwner((String)targetPly);
						
						// update each panels
						updatePanelInfo("player", plyNo);
						updatePanelInfo("property", proNo);
						updatePanelInfo("control", plyNo);
					} else if(price > players[targetPlyNo].getCash()) {
						JOptionPane.showMessageDialog(null, "The other party doesn't have enough cash to pay", 
								  					  "Error", JOptionPane.ERROR_MESSAGE);
					}
				}
			}
		}
		
		/**
		 * Sell buildings on a property, remove houses and hotels evenly 
		 * @param plyNo  current player number 
		 * @param color  the color group of the player owned 
		 */
		public void sellHouse(int plyNo, String color) {
			int mortgage = 0;
			int group = blocks.findGroupNo(color);
			int currentHouse = property[group].getHouse()+property[group+1].getHouse()+property[group+2].getHouse();
			int currentHotel = property[group].getHotel()+property[group+1].getHotel()+property[group+2].getHotel();
			
			Object[] houseOption = {1, 2, 3, 4, 5, 6};
			Object numHouse = JOptionPane.showInputDialog(null, "You have " + currentHouse + " house" + "and "
					+ currentHotel + " hotel on this color group\n. How many buildings do you want to sell?", 
					"Selling Houses", JOptionPane.INFORMATION_MESSAGE, null, houseOption, houseOption[0]);
			
			/* selling buildings evenly*/
			if(numHouse != null && (int)numHouse <= (currentHouse + currentHotel)) {
				int nBuildings0;
				int nBuildings1;
				int nBuildings2;
				for(int i=0; i<(int)numHouse; i++) {
					nBuildings0 = property[group].getHouse()+property[group].getHotel();
					nBuildings1 = property[group+1].getHouse()+property[group+1].getHotel();
					nBuildings2 = property[group+2].getHouse()+property[group+2].getHotel();
					if(nBuildings2 != 0 && nBuildings2 >= nBuildings1) {
						if(property[group+2].getHotel() > 0) {property[group+2].reduceHotel(); numOfHotel++;}
						else {property[group+2].reduceHouse(); numOfHouse++;}
						mortgage += property[group+2].getHouseCost()/2;
					} else if(nBuildings1 != 0 && nBuildings1 >= nBuildings0) {
						if(property[group+1].getHotel() > 0) {property[group+1].reduceHotel(); numOfHotel++;}
						else {property[group+1].reduceHouse(); numOfHouse++;}
						mortgage += property[group+1].getHouseCost()/2;
					} else {
						if(property[group].getHotel() > 0) {property[group].reduceHotel(); numOfHotel++;}
						else {property[group].reduceHouse(); numOfHouse++;}
						mortgage += property[group].getHouseCost()/2;
					} 
				}
				players[plyNo].addCash(mortgage);
				updatePanelInfo("player", plyNo);
				updatePanelInfo("property", group+2);
				updatePanelInfo("control", plyNo);
			} else if((int)numHouse > (currentHouse + currentHotel)) {
				JOptionPane.showMessageDialog(null, "You don't have so many buildings"
											  + "to sell!", "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		/**
		 * Buy house/hotel on a property 
		 * @param plyNo current player number
		 * @param color target color group  
		 */
		public void buyHouse(int plyNo, String color) {
			Object[] houseOption = {1, 2, 3, 4, 5, 6};
			Object numHouse = JOptionPane.showInputDialog(null, 
					"How many houses you want to buy?", "Buying house",
					JOptionPane.INFORMATION_MESSAGE, null, 
					houseOption, houseOption[0]);
			
			/* if there're enough houses surplus*/
			if(numHouse != null && (int)numHouse <= numOfHouse) {
				int group = blocks.findGroupNo(color);
				int currentHouse = property[group].getHouse()+property[group+1].getHouse()+property[group+2].getHouse();
				
				// if not Airport/Utilities
				if(group >= 0) {   
					if(currentHouse == 12 || property[group].getHotel()>0 || property[group+1].getHotel()>0 || property[group+2].getHotel()>0) {
						// buy hotel if houses if full 
						int res = JOptionPane.showConfirmDialog(null, "Your houses is full, do you want buy hotels?"
																,"Buying hotel", JOptionPane.YES_NO_OPTION);
						if(res == JOptionPane.YES_OPTION)
							erectHotel(group, plyNo);
					} else if((int)numHouse + currentHouse <= 12) {  // if houses is not full on the color group
						erectHouse(group, (int)numHouse, plyNo);
					} else {
						JOptionPane.showMessageDialog(null, "You can only buy " + (12-currentHouse) + 
													 ((12-currentHouse)>1?" houses":" house") +
													  " on this color group", "Error", JOptionPane.ERROR_MESSAGE);
					}
				} else {
					JOptionPane.showMessageDialog(null, "Can not buy buildings on Airport/Utilities", 
												  "Error", JOptionPane.ERROR_MESSAGE);
				}
			} else if(numHouse != null && (int)numHouse > numOfHouse) {
				JOptionPane.showMessageDialog(null, "Not engough houses surplus", 
						  "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		/**
		 * erect houses to the properties of a color group evenly 
		 * @param group  target color group 
		 * @param numHouse  amount of houses being erected 
		 * @param plyNo current player number 
		 */
		public void erectHouse(int group, int numHouse, int plyNo) {
			int cost = 0;
			/* calculate cost*/
			for(int i=0; i<numHouse; i++) {
				if(property[group].getHouse() == 0) {
					cost += property[group].getHouseCost();
				} else if(property[group+1].getHouse() < property[group].getHouse()) {
					cost += property[group+1].getHouseCost();
				} else if(property[group+2].getHouse() < property[group+1].getHouse()) {
					cost += property[group+2].getHouseCost();
				} else {
					cost += property[group].getHouseCost();
				}
			}
			
			int res = JOptionPane.showConfirmDialog(null, "You need pay " + cost + "�� to buy " +
													numHouse + (numHouse > 1? " houses":" house") +
													", proceed?", "Buying house", JOptionPane.YES_NO_OPTION);
			/* if the player is able to pay*/
			if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() >= cost) {
				for(int i=0; i<numHouse; i++) {  // erect house evenly 
					if(property[group].getHouse() == 0) {
						property[group].addHouse();
					} else if(property[group+1].getHouse() < property[group].getHouse()) {
						property[group+1].addHouse();
					} else if(property[group+2].getHouse() < property[group+1].getHouse()) {
						property[group+2].addHouse();
					} else {
						property[group].addHouse();
					}
				}
				numOfHouse -= numHouse;
				players[plyNo].reduceCash(cost);
				updatePanelInfo("player", plyNo);
				updatePanelInfo("property", group);
				updatePanelInfo("control", plyNo);
			} else if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() < cost) {
				JOptionPane.showMessageDialog(null, "You don't have enough cash!", "Can not purchase",
		                JOptionPane.ERROR_MESSAGE);
			} 
		}
		
		/**
		 * erect hotels to the properties of a color group evenly 
		 * @param group  target color group 
		 * @param plyNo current player number 
		 */
		public void erectHotel(int group, int plyNo) {
			int currentHotel = property[group].getHotel()+property[group+1].getHotel()+property[group+2].getHotel();
			Object[] houseOption = {1, 2, 3};
			Object numHotel = JOptionPane.showInputDialog(null, 
					"How many hotels you want to buy?", "Buying hotel",
					JOptionPane.INFORMATION_MESSAGE, null, 
					houseOption, houseOption[0]);
			
			/* if there're enough hotels surplus*/
			if(numHotel != null && (int)numHotel <= numOfHotel) {
				if(currentHotel + (int)numHotel <= 3) {
					int cost = 0;
					/* calculate cost*/
					for(int i=0; i<(int)numHotel; i++) {
						if(property[group].getHotel() == 0) {
							cost = cost + property[group].getHouseCost();
						} else if(property[group+1].getHotel() == 0) {
							cost = cost + property[group+1].getHouseCost();
						} else {
							cost = cost + property[group+2].getHouseCost();
						} 
					}
					
					int res = JOptionPane.showConfirmDialog(null, "You need pay " + cost + "�� to buy " +
															numHotel + ((int)numHotel > 1? " hotels":" hotel") +
															", proceed?", "Buying hotel", JOptionPane.YES_NO_OPTION);
					/* if the player is able to pay*/
					if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() >= cost) {
						for(int i=0; i<(int)numHotel; i++) {  // erect house evenly 
							if(property[group].getHotel() == 0) {
								property[group].addHotel();
							} else if(property[group+1].getHotel() == 0) {
								property[group+1].addHotel();
							} else {
								property[group+2].addHotel();
							}
						}
						numOfHotel -= (int)numHotel;
						players[plyNo].reduceCash(cost);
						updatePanelInfo("player", plyNo);
						updatePanelInfo("property", group);
						updatePanelInfo("control", plyNo);
					} else if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() < cost) {
						JOptionPane.showMessageDialog(null, "You don't have enough cash!", "Can not purchase",
				                JOptionPane.ERROR_MESSAGE);
					} 
				} else {
					JOptionPane.showMessageDialog(null, "You can only buy " + (3-currentHotel) + 
							 ((3-currentHotel)>1?" hotels":" hotel") +
							  " on this color group", "Error", JOptionPane.ERROR_MESSAGE);
				}
			} else if(numHotel != null && (int)numHotel > numOfHotel) {
				JOptionPane.showMessageDialog(null, "Not engough hotels surplus", 
						  "Error", JOptionPane.ERROR_MESSAGE);
			}
		}
		
		/**
		 * Chance card activities
		 * @param cardNo which card is drew
		 */
		public void drawChanceCard(int cardNo, int plyNo, int blockNo) {
			JOptionPane.showConfirmDialog(null, "You get a Chance Card !\n"+ cards.getChanceInfo(cardNo), "A Chance Card",
										  JOptionPane.YES_NO_OPTION);

			switch(cardNo) {
			case 0:
				mainPanel.plyCalculation(0,plyNo);
				players[plyNo].setBlock(0);
				players[plyNo].addCash(200);
				updatePanelInfo("player", plyNo);
				break;
			case 1:
				if (blockNo > 23) {
					players[plyNo].addCash(200);
				}
				mainPanel.plyCalculation(23,plyNo);
				players[plyNo].setBlock(23);
				proActivity(23, plyNo);
				updatePanelInfo("property", 14);
				break;
			case 2:
				// "Advance token to Energy Company. If unowned, you may buy it from the Bank."
				// "If owned, throw dice and pay owner a total 10 times the amount thrown.";				
				int dicePoint = dice();
				if (blockNo > 17 && blockNo < 38) {
				mainPanel.plyCalculation(38,plyNo);
				players[plyNo].setBlock(38);
					if (property[27].getOwner() != players[plyNo].getName() && property[27].getOwner() != "Null") {
						players[plyNo].reduceCash(dicePoint*10); 
						players[blocks.findPlyNo(property[27].getOwner())].addCash(dicePoint*10);
						updatePanelInfo("property", 27);
						updatePanelInfo("player", plyNo);
					} else {
						proActivity(38, plyNo);
					}	
				} else {
						mainPanel.plyCalculation(17,plyNo);
						players[plyNo].setBlock(17);
						if (property[26].getOwner() != players[plyNo].getName() && property[26].getOwner() != "Null") {
							players[plyNo].reduceCash(dicePoint*10); 
							players[blocks.findPlyNo(property[26].getOwner())].addCash(dicePoint*10);
							updatePanelInfo("property", 26);
							updatePanelInfo("player", plyNo);
						} else { 
							proActivity(17, plyNo);					
						}
				}
				break;
			case 3:
				// To the nearest airport, if owned, pay twice rent				
				if (blockNo > 25 || blockNo < 5) {
					mainPanel.plyCalculation(5,plyNo);
					players[plyNo].setBlock(5);
					if (property[24].getOwner() != players[plyNo].getName() && property[24].getOwner() != "Null" ) {
						int rent = property[5].getRent();
						players[plyNo].reduceCash(rent*2); 
						players[blocks.findPlyNo(property[24].getOwner())].addCash(rent*2);
						updatePanelInfo("property", 24);
						updatePanelInfo("player", plyNo);
						updatePanelInfo("player", blocks.findPlyNo(property[24].getOwner()));
					} else {
						proActivity(5, plyNo);
					}				
				} else {
					mainPanel.plyCalculation(25,plyNo);
					players[plyNo].setBlock(25);
					if (property[25].getOwner() != players[plyNo].getName() && property[25].getOwner() != "Null") {
						int rent = property[25].getRent();
						players[plyNo].reduceCash(rent*2); 
						players[blocks.findPlyNo(property[25].getOwner())].addCash(rent*2);
						updatePanelInfo("property", 25);
						updatePanelInfo("player", plyNo);
						updatePanelInfo("player", blocks.findPlyNo(property[25].getOwner()));
					} else {
						proActivity(25,plyNo);				
					}
				}
				break;
			case 4:
				players[plyNo].addCash(50);
				updatePanelInfo("player", plyNo);
				break;
			case 5:
				players[plyNo].getJailFreeCard();
				updatePanelInfo("player", plyNo);
				break;
			case 6:
				mainPanel.plyCalculation(10,plyNo);
				players[plyNo].setBlock(10);
				players[plyNo].setState();
				updatePanelInfo("player", plyNo);
				break;
			case 7:
				players[plyNo].addCash(100);
				updatePanelInfo("player", plyNo);
				break;
			case 8:
				int ownedProperty[] = players[plyNo].getProperty();
				int nHouse=0, nHotel=0;
				for(int i=0; i<PublicData.numProperty; i++) {
					if(ownedProperty[i] > 0) {
						nHouse += property[i].getHouse();
						nHotel += property[i].getHotel();
					}
				}
				players[plyNo].reduceCash(nHouse*25 + nHotel*100);
				updatePanelInfo("player", plyNo);
				break;
				//For each house pay $25, For each hotel pay $100."
			case 9:
				players[plyNo].reduceCash(100);
				updatePanelInfo("player", plyNo);
				break;
			case 10:
				if (blockNo > 5) {
					players[plyNo].addCash(200);
				}
				mainPanel.plyCalculation(5,plyNo);
				players[plyNo].setBlock(5);
				proActivity(5, plyNo);
				updatePanelInfo("property", 24);
				break;
			case 11:
				mainPanel.plyCalculation(39,plyNo);
				players[plyNo].setBlock(39);
				proActivity(39, plyNo);
				updatePanelInfo("property", 23);
				break;
			case 12:
				for(int i=0;i<numOfPly;i++) {
					if (i == plyNo) {
						players[i].reduceCash((numOfPly-1)*50);
					} else {
						players[i].addCash(50);
					}
					updatePanelInfo("player", plyNo);
				}
				break;
			case 13:
				players[plyNo].addCash(150);
				updatePanelInfo("player", plyNo);
				break;
			case 14:
				mainPanel.plyCalculation(1,plyNo);
				players[plyNo].setBlock(1);
				proActivity(1, plyNo);
				updatePanelInfo("property", 0);
				break;
			case 15:
				players[plyNo].addCash(100);
				updatePanelInfo("player", plyNo);
				break;
				}
			}
		
		/**
		 * Fate card activities
		 * @param cardNo which card is drew
		 */
		public void drawFateCard(int cardNo,int plyNo) {
			JOptionPane.showConfirmDialog(null, "You get a Fate Card !\n"+ cards.getFateInfo(cardNo), "A Fate Card",
					  JOptionPane.YES_NO_OPTION);
			switch(cardNo) {
			case 0:
				mainPanel.plyCalculation(0,plyNo);
				players[plyNo].addCash(200);
				break;
			case 1:
				players[plyNo].addCash(200);
				break;
			case 2:
				players[plyNo].reduceCash(50);
				break;
			case 3:
				players[plyNo].addCash(50);
				break;
			case 4:
				players[plyNo].getJailFreeCard();
				break;
			case 5:
				inJail(plyNo);
				break;
			case 6:
				for(int i=0; i<numOfPly; i++) {
						players[i].reduceCash(50);
					}

				break;
			case 7:
				players[plyNo].addCash(100);
				break;
			case 8:
				players[plyNo].addCash(20);
				break;
			case 9:
				for(int i=0; i<numOfPly; i++) {
					if(i == plyNo) {
						players[i].addCash((numOfPly-1)*10);
					} else {
						players[i].reduceCash(10);
					}
				}
				break;
			case 10:
				players[plyNo].addCash(100);
				break;
			case 11:
				players[plyNo].reduceCash(50);
				break;
			case 12:
				players[plyNo].reduceCash(50);
				break;
			case 13:
				players[plyNo].addCash(25);
				break;
			case 14:
				/* pay 40$ per house and 115$ per hotel.*/
				int ownedProperty[] = players[plyNo].getProperty();
				int nHouse=0, nHotel=0;
				for(int i=0; i<PublicData.numProperty; i++) {
					if(ownedProperty[i] > 0) {
						nHouse += property[i].getHouse();
						nHotel += property[i].getHotel();
					}
				}
				players[plyNo].reduceCash(nHouse*40 + nHotel*115);
				updatePanelInfo("player", plyNo);
				break;
			case 15:
				players[plyNo].addCash(10);
				break;
			}
			updatePanelInfo("player", plyNo);
		}
		
		/**
		 * Activities when the player is in jail 
		 * @param plyNo  current player number 
		 */
		public void inJail(int plyNo) {
			if(!players[plyNo].getState()) {  // first time in jail
				mainPanel.plyCalculation(10,plyNo);
				players[plyNo].setState();
				players[plyNo].resetDiceHistory("odd"); 
				players[plyNo].setBlock(10);
				
				JOptionPane.showMessageDialog(null, "You are in JAIL !!!", 
						  "Error", JOptionPane.ERROR_MESSAGE);
				updatePanelInfo("player", plyNo);
			} else {
				if(players[plyNo].viewJailCard() > 0) {
					int res = JOptionPane.showConfirmDialog(null, "You are in Jail, do you want"
															+ "use Jail Free Card?", "Jail Free",
															JOptionPane.YES_NO_OPTION);

						if(res == JOptionPane.YES_OPTION) {
							players[plyNo].setState();
							players[plyNo].resetDiceHistory("odd");
						} else {  // if not use jail card then ask for bail bonds 
							int res2 = JOptionPane.showConfirmDialog(null, "Do you want pay 50�� Bail Bonds?", "Bail",
				                    	JOptionPane.YES_NO_OPTION);
							if(res2 == JOptionPane.YES_OPTION && players[plyNo].getCash() >= 100) {
								players[plyNo].bail();
								players[plyNo].resetDiceHistory("even");
							} else if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() < 100) {
								JOptionPane.showMessageDialog(null, "You don't have enough to pay Bail Bonds", 
										  "Unable to Bail", JOptionPane.ERROR_MESSAGE);
							}
						}
				} else {  // if no jail card, ask for bail bonds 
					if(!players[plyNo].getBailState()) {
						int res = JOptionPane.showConfirmDialog(null, "Do you want pay 50�� Bail Bonds?", "Bail",
		                    	JOptionPane.YES_NO_OPTION);
						if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() >= 100) {
							players[plyNo].bail();
							players[plyNo].resetDiceHistory("even");
						} else if(res == JOptionPane.YES_OPTION && players[plyNo].getCash() < 100) {
							JOptionPane.showMessageDialog(null, "You don't have enough to pay Bail Bonds", 
									  "Unable to Bail", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
			}
		}
		
		/**
		 * Update panels info 
		 * @param panel target panel
		 * @param No player/property number 
		 */
		public void updatePanelInfo(String panel, int No) {
			switch(panel) {
			case "player":
				Object[] plyInfo = (Object[]) players[No].getPlyInfo();		
				playerPanel.setPlyInfo((String)plyInfo[0], (int)plyInfo[1], (int)plyInfo[2], 
										(String)plyInfo[3], (boolean)plyInfo[4]);
				break;			
			case "property":
				Object[] proInfo = (Object[]) property[No].getProInfo();
				propertyPanel.setProInfo((String)proInfo[0], (int)proInfo[1], (int)proInfo[2], 
									    (String)proInfo[3]);
				break;			
			case "control":
				setPubInfo(players[No].getName(), numOfHouse, numOfHotel, numOfPro);
				break;		
			default:
				break;
			}
		}
		
		/**
		 * Update bid area info during bid activity 
		 * @param bid  if the bid process is running 
		 */
		public void updateBid(boolean bid) {
			if(bid) {
				bidValue = 50;
				currentBid.setForeground(Color.red);
				btnView.setForeground(Color.black);
				btnBid.setForeground(Color.black);
				btnEndBid.setForeground(Color.black);
				currentBid.setText("Current Bid: " + String.valueOf(bidValue) + "��");
				
				/* Disable other buttons on control panel*/
				btnSearch.setForeground(Color.gray);
				btnGo.setForeground(Color.gray);
				btnSellPro.setForeground(Color.gray);
				btnSellHouse.setForeground(Color.gray);
				btnBuyHouse.setForeground(Color.gray);
			} else {
				bidValue = 50;
				currentBid.setForeground(Color.gray);
				btnView.setForeground(Color.gray);
				btnBid.setForeground(Color.gray);
				btnEndBid.setForeground(Color.gray);
				currentBid.setText("Current Bid: " + String.valueOf(bidValue) + "��");
				
				/* Open other buttons on control panel*/
				btnSearch.setForeground(Color.black);
				btnGo.setForeground(Color.black);
				btnSellPro.setForeground(Color.black);
				btnSellHouse.setForeground(Color.black);
				btnBuyHouse.setForeground(Color.black);
			}
		}
		/* ---------------------------------------------------------------------------------------*/
	}
}
