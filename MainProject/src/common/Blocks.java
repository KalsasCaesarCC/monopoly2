package common;

import javax.swing.JOptionPane;

/**
 * Class for normal property activities 
 * @author Cheng Chen
 *
 */
public class Blocks {
	
	/**
	 * Create blocks 
	 */
	public Blocks() {
		
	}
	
	/**
	 * Return property number stored in database 
	 * @param blockNo block number 
	 * @return property number 
	 */
	public int findProNo(int blockNo) {
		int proNo = 0;
		for(int i=0; i<PublicData.numProperty; i++) {
			if(blockNo == (int)PublicData.proInfo[i][PublicData.proIndex.BLOCK.ordinal()]) {
				proNo = i;
				break;
			}
		}
		return proNo;
	}
	
	/**
	 * Return property owner plyNo  
	 * @param owner property owner
	 * @return player number stored in database 
	 */
	public int findPlyNo(String plyName) {
		switch(plyName) {
		case "Magallan": return 0;
		case "Silver": return 1;
		case "Texas": return 2;
		case "Lapland": return 3;
		case "Warfarin": return 4;
		case "Kaltsit": return 5;
		}
		return -1;
	}
	
	/**
	 * Return the sequence number of a color group
	 * @param color
	 * @return color group number 
	 */
	public int findGroupNo(String color) {
		switch(color) {
		case "pink": return 0; 
		case "green": return 3; 
		case "blue": return 6; 
		case "yellow": return 9; 
		case "gray": return 12; 
		case "red": return 15; 
		case "violet": return 18; 
		case "brown": return 21; 
		case "airport": return -1;
		case "energy": return -2; 
		}
		return -3;
	}
	
	/**
	 * Return type of property
	 * @param blockNo block number 
	 * @return property type
	 */
	public String findProType(int blockNo) {
		switch(blockNo) {
		case 5:
		case 25:
			return "airport";
		case 18:
		case 38:
			return "energy";
		default:
			return "ordinary";
		}
	}
	
	/**
	 * Purchase a property 
	 * @param price price of the property
	 * @param name name of the property
	 */
	public int proPurchase(int price, String name) {
		int res = JOptionPane.showConfirmDialog(null, "Do you want buy the property on: "
				                                + name + "?\nPrice: " + price + "��", "Buy a property",
				                                JOptionPane.YES_NO_OPTION);
		
		if(res == JOptionPane.YES_OPTION)
			return price;
		else
			return 0;
	}
	
	/**
	 * Show message that the player can not purchase properties
	 */
	public void canNotPurchase() {
		JOptionPane.showMessageDialog(null, "You can notpurchase the property "
									  + "cause you don't have enough cash", 
									  "Can not purchase", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Show message of rent payment
	 * @param rent amount of rent
	 * @param owner name of property owner
	 * @return amount of rent 
	 */
	public int payRent(int rent, String owner) {
		JOptionPane.showMessageDialog(null, "You need pay rent " + rent + "��"
                + " to player: " + owner, "Pay rent",
                JOptionPane.ERROR_MESSAGE);
		
		return rent;
	}
	
}
