package common;

import static org.junit.Assert.*;

import org.junit.Test;

public class Blocks_Test {

	@Test
	/**
	 * Test if the returned property number correct 
	 * given a property block number 
	 */
	public void test_findProNo() {
		Blocks block = new Blocks();
		int res = 0; 
		int exp = 3;
		
		res = block.findProNo(6);
		assertEquals(exp, res);
	}

}
