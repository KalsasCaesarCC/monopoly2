package common;

import static org.junit.Assert.*;

import org.junit.Test;

public class Card_Test {

	@Test
	/**
	 * Test if return a card number between 0 - 15
	 * Represent all "get" function test in this class
	 */
	public void test_getCard() {
		Card cards = new Card();
		int res = 0; 
		int exp = 20;
		
		res = cards.getChanceCard();
		if(res >=0 && res<=15)
			exp = res;
		
		assertEquals(exp, res);
	}

}
