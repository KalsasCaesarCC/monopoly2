package common;

import static org.junit.Assert.*;

import org.junit.Test;

public class Players_Test {
	
	@Test
	/**
	 * player go to jail if throws doubles three times in succession.
	 */
	public void test_isGoJail() {
		Players player = new Players("Tester");
		boolean res = false; 
		boolean exp = true;
		
		for(int i=0; i<3; i++) {
			res = player.isGoJail(2);
		}
		assertEquals(exp, res);
	}
	
	@Test
	/**
	 * player set free if throw doubles three times in succession when in jail.
	 */
	public void test_isGoJail2() {
		Players player = new Players("Tester");
		boolean res = true; 
		boolean exp = false;
		player.setState();
		
		for(int i=0; i<3; i++) {
			res = player.isGoJail(2);
		}
		assertEquals(exp, res);
	}
	
	@Test
	/**
	 * player set free if he/she paid bail and throw double after payment.
	 */
	public void test_isGoJail3() {
		Players player = new Players("Tester");
		boolean res = true; 
		boolean exp = false;
		player.setState();
		player.bail();
		
		res = player.isGoJail(2);
		assertEquals(exp, res);
	}
	
	@Test
	/**
	 * if the block calculation is correct.
	 */
	public void test_blockCalculation() {
		Players player = new Players("Tester");
		int res = 0; 
		int exp = 15;
		player.setBlock(5);
		
		res = player.blockCalculation(10);
		assertEquals(exp, res);
		
		// if block greater than 40 blocks 
		player.setBlock(39);
		res = player.blockCalculation(10);
		exp = 9;
		assertEquals(exp, res);
	}

}
