package common;

/**
 * Storing common data 
 * @author Cheng Chen 
 *
 */
public class PublicData {
	
	public enum proIndex {
		NAME,
		PRICE,
		RENT,
		HOUSE1,
		HOUSE2,
		HOUSE3,
		HOUSE4,
		HOTEL,
		HCOST,
		BLOCK,
		COLOR,
		NHOUSE,
		NHOTEL,
		OWNER;
	}
	
	// properties information 
	public final static Object[][] proInfo = {{"Greece", 90, 14, 70, 200, 550, 750, 950, 100, 1, "pink", 0, 0, "Null"},
											 {"Thailand", 90, 14, 70, 200, 550, 750, 950, 100, 2, "pink", 0, 0, "Null"},
											 {"Italy", 100, 16, 80, 220, 600, 800, 1000, 100, 3, "pink", 0, 0, "Null"},
											 {"Ireland", 120, 20, 100, 300, 750, 925, 1100, 150, 6, "green", 0, 0, "Null"},
											 {"Austrilia", 110, 18, 90, 250, 700, 875, 1050, 150, 7, "green", 0, 0, "Null"},
											 {"Japan", 60, 8, 40, 100, 300, 450, 600, 50, 9, "green", 0, 0, "Null"},
											 {"USA", 110, 18, 90, 250, 700, 875, 1050, 150, 11, "blue", 0, 0, "Null"},
											 {"Singapore", 200, 50, 200, 600, 1400, 1700, 2000, 200, 12, "blue", 0, 0, "Null"},
											 {"Spain", 50, 6, 30, 90, 270, 400, 550, 50, 14, "blue", 0, 0, "Null"},
											 {"UK", 130, 22, 110, 330, 800, 975, 1150, 150, 16, "yellow", 0, 0, "Null"},
											 {"Iceland", 130, 22, 110, 330, 800, 975, 1150, 150, 17, "yellow", 0, 0, "Null"},
											 {"Canada", 70, 10, 50, 150, 450, 625, 750, 100, 19, "yellow", 0, 0, "Null"},
											 {"Sweden", 70, 10, 50, 150, 450, 625, 750, 100, 21, "gray", 0, 0, "Null"},
											 {"France", 80, 12, 60, 180, 500, 700, 900, 100, 22, "gray", 0, 0, "Null"},
											 {"Finland", 140, 24, 120, 360, 850, 1025, 1200, 150, 23, "gray", 0, 0, "Null"},
											 {"Korea", 50, 6, 30, 90, 270, 400, 550, 50, 26, "red", 0, 0, "Null"},
											 {"Russia", 30, 4, 20, 60, 180, 320, 450, 50, 27, "red", 0, 0, "Null"},
											 {"China", 175, 35, 175, 500, 1100, 1300, 1500, 200, 29, "red", 0, 0, "Null"},
											 {"Holland", 30, 2, 10, 30, 90, 160, 250, 50, 31, "violet", 0, 0, "Null"},
											 {"Germany", 150, 26, 130, 390, 900, 1100, 1275, 200, 32, "violet", 0, 0, "Null"},
											 {"Portugal", 150, 26, 130, 390, 900, 1100, 1275, 200, 34, "violet", 0, 0, "Null"},
											 {"Brazil", 160, 28, 150, 450, 1000, 1200, 1400, 200, 36, "brown", 0, 0, "Null"},
											 {"Norway", 80, 12, 60, 180, 500, 700, 900, 100, 37, "brown", 0, 0, "Null"},
											 {"Turkey", 30, 2, 10, 30, 90, 160, 250, 50, 39, "brown", 0, 0, "Null"},
											 {"BJ Airport", 100, 25, 50, 100, 200, 0, 0, 0, 5, "airport", 0, 0, "Null"},
											 {"LON Airport", 100, 25, 50, 100, 200, 0, 0, 0, 25, "airport", 0, 0, "Null"},
											 {"WaterCo", 75, 0, 0, 0, 0, 0, 0, 0, 18, "energy", 0, 0, "Null"},
											 {"EleCo", 75, 0, 0, 0, 0, 0, 0, 0, 38, "energy", 0, 0, "Null"}};
	
	public final static String[] plyName = {"Magallan", "Silver", "Texas", "Lapland", "Warfarin", "Kaltsit"};
	
	public final static int[] plyInitX = {875, 875, 875, 905, 905, 905};
	public final static int[] plyInitY = {875, 900, 925, 875, 900, 925};
	
	public final static int numProperty = 28;
	public final static int numHouse = 32;
	public final static int numHotel = 12;
	public final static int plyInitCash = 1000;
	public final static int salary = 200;
	public final static int tax = 200;
	
}
