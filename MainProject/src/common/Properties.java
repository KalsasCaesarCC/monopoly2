package common;

import java.util.Random;

/**
 * Represents a property 
 * @author Cheng Chen 
 *
 */
public class Properties {
	private String Name, Owner;
	private int numOfHouse, numOfHotel, Price, Rent,
	            House1, House2, House3, House4, Hotel, Hcost;
	
	/**
	 * Creates a property 
	 * @param name name of property 
	 * @param price property fee
	 * @param rent property rent
	 * @param house1 rent with 1 house
	 * @param house2 rent with 2 houses
	 * @param house3 rent with 3 houses
	 * @param house4 rent with 4 houses
	 * @param hotel rent with hotel
	 * @param price of each house/hotel
	 * @param block block number on chess board 
	 * @param cg color group 
	 * @param nHouse number of house built
	 * @param nHotel number of hotel built
	 * @param owner name of owner 
	 */
	public Properties(String name, int price, int rent, int house1,
			          int house2, int house3, int house4, int hotel,
			          int hcost, int block, String cg, int nHouse, 
			          int nHotel, String owner) {
		
		// static variables 
		Name = name;
		Price = price;
		Rent = rent;
		House1 = house1;
		House2 = house2;
		House3 = house3;
		House4 = house4;
		Hotel = hotel;
		Hcost = hcost;
		
		// dynamic variables
		Owner = owner;
		numOfHouse = nHouse;
		numOfHotel = nHotel;
	}
	
	/**
	 * Return properties name
	 * @return properties name
	 */
	public String getName() {
		return Name;
	}
	
	/**
	 * Return properties owner
	 * @return properties owner
	 */
	public String getOwner() {
		return Owner;
	}
	
	/**
	 * Update properties owner
	 * @param new properties owner
	 */
	public void setOwner(String owner) {
		Owner = owner;
	}
	
	/**
	 * Return properties price
	 * @return properties price
	 */
	public int getPrice() {
		return Price;
	}
	
	/**
	 * Return calculated rent of a property
	 * @return rent value
	 */
	public int getRent() {
		if (Rent == 0)
			return new Random().nextInt(12) + 1;  // energy property
		else {
			switch(numOfHouse) {
				case 0:
					if(numOfHotel == 1)
						return Hotel;
					else
						return Rent;
				case 1:
					return House1;
				case 2:
					return House2;
				case 3:
					return House3;
				case 4:
					return House4;
			}
			return -1;
		}
	}
	
	/**
	 * Return number of houses on the property 
	 * @return number of houses
	 */
	public int getHouse() {
		return numOfHouse;
	}
	
	/**
	 * Return money cost of building a house 
	 * same price for hotel 
	 * @return building cost 
	 */
	public int getHouseCost() {
		return Hcost;
	} 
	
	/**
	 * Add number of houses on the property 
	 */
	public void addHouse() {
		numOfHouse++;
	}
	
	/**
	 * Reduce number of houses on the property 
	 */
	public void reduceHouse() {
		numOfHouse--;
	}
	
	/**
	 * Return number of hotels on the property 
	 * @return number of hotel 
	 */
	public int getHotel() {
		return numOfHotel;
	}
	
	/**
	 * Add hotel to the property 
	 */
	public void addHotel() {
		numOfHouse = 0;
		numOfHotel = 1;
	}
	
	/**
	 * Remove the hotel from the property 
	 */
	public void reduceHotel() {
		numOfHotel = 0;
	}
	
	/**
	 * Return property information
	 * @return property information 
	 */
	public Object getProInfo() {
		Object[] proInfo = {Name, numOfHouse, numOfHotel, Owner};
		return proInfo;
	}
}

