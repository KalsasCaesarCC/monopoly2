package common;

import java.util.Arrays;

import javax.swing.JOptionPane;

/**
 * Represents a player 
 * @author Cheng Chen
 *
 */
public class Players {
	private String Name;
	private String OwnedProName;
	private int Cash, JailCard, Block;
	private int dice[];
	private int OwnedProperty[];
	private int OwnedColor[];
	private String OwnedGroup[];
	private boolean isPrision;
	private boolean isBail;
	
	/**
	 * Create a player 
	 * @param name name of player 
	 */
	public Players(String name) {
		Name = name;
		OwnedProName = "";
		Cash = PublicData.plyInitCash;
		JailCard = 0;
		isPrision = false;
		isBail = false;
		Block = 0;
		dice = new int[2];  // store last 2 dice value
		OwnedProperty = new int[PublicData.numProperty];
		OwnedColor = new int[10];
		OwnedGroup = new String[10];
		
		Arrays.fill(OwnedProperty, -1);
		Arrays.fill(dice, 13);
	}
	
	/**
	 * Decide if the player should go jail or free 
	 * @param dicePoint  current dice point
	 * @return true if the player should go jail, false when the player is free
	 */
	public boolean isGoJail(int dicePoint) {
		if(!isPrision) {  // if the player is not in jail 
			if(dice[0]%2==0 && dice[1]%2==0 && dicePoint%2==0) {  // go to jail
				resetDiceHistory("odd");  // reset dice storage 
				return true;
			} else {  // save the last two dice results 
				dice[1] = dice[0];
				dice[0] = dicePoint;
				return false;
			}
		} else if(isBail) {  // if the player is in jail and paid bail 
			if(dicePoint%2==0) {  // free to go
				resetDiceHistory("odd");
				isBail = false;
				return false;
			} else if(dice[0]%2!=0 && dice[1]%2!=0 && dicePoint%2!=0) {  // pay 50 and free to go 
				reduceCash(50);
				resetDiceHistory("odd");
				isBail = false;
				return false;
			} else {  // save the last two dice results 
				dice[1] = dice[0];
				dice[0] = dicePoint;
				return true;
			}
		} else {  // if the player is in jail but didn't paid bail 
			if(dice[0]%2==0 && dice[1]%2==0 && dicePoint%2==0) {  // free to go
				resetDiceHistory("odd");
				return false;
			} else {
				dice[1] = dice[0];
				dice[0] = dicePoint;
				return true;
			}
		}
	}
	
	/**
	 * Reset stored dice results if the player is in jail or set free
	 * @param type  type of reset, set "even" if paid bail, otherwise set "odd"
	 */
	public void resetDiceHistory(String type) {
		if(type == "even") {
			Arrays.fill(dice, 14);
		} else if(type == "odd") {
			Arrays.fill(dice, 13);
		}
	}
	
	/**
	 * Player paid the bail 
	 */
	public void bail() {
		isBail = true;
	}
	
	public boolean getBailState() {
		return isBail;
	}
	
	/**
	 * Calculate position of player, return the 
	 * number of block where the player should
	 * be  
	 * @param dicePoint dice point
	 * @return number of block 
	 */
	public int blockCalculation(int dicePoint) {
		boolean isJail = isGoJail(dicePoint);
		if(!isPrision && isJail) {  // go to jail
			return 30;
		} else if(isPrision && !isJail) {
			// free to go
			JOptionPane.showConfirmDialog(null, "You are free to go!", "Free",
                    JOptionPane.YES_NO_OPTION);
			setState();
			isBail = false;
		} else if(isPrision && isJail) {
			return 10;
		}
		
		Block += dicePoint;
		
		if(Block > 39) {
			Block -= 40;
			addCash(PublicData.salary);
			JOptionPane.showConfirmDialog(null, "Get salary 200��!", "Salary",
                    JOptionPane.YES_NO_OPTION);
		}
		
		return Block;
	}
	
	/**
	 * Move player directly to some blocks
	 * @param blockNo  number of target block
	 */
	public void setBlock(int blockNo) {
		Block = blockNo;
	}
	
	/**
	 * Return players name 
	 * @return name of player 
	 */
	public String getName() {
		return Name;
	}
	
	/**
	 * Return player current cash 
	 * @return value of cash 
	 */
	public int getCash() {
		return Cash;
	}
	
	/**
	 * Return owned color group of the player
	 * @return owned color group 
	 */
	public String[] getColorGroup() {
		return OwnedGroup;
	}
	
	/**
	 * Return owned property of the player
	 * @return owned property 
	 */
	public int[] getProperty() {
		return OwnedProperty;
	}
	
	/**
	 * Return players information
	 * @return player information 
	 */
	public Object getPlyInfo() {
		Object[] plyInfo = {Name, Cash, JailCard, OwnedProName, isPrision};
		return plyInfo;
	}
	
	/**
	 * Add players cash
	 * @param cash value of cash in 
	 */
	public void addCash(int cash) {
		Cash += cash;
	}
	
	/**
	 * Reduce players cash
	 * @param cash value of cash out
	 */
	public void reduceCash(int cash) {
		Cash -= cash;
	}
	
	/**
	 * Add owned properties
	 */
	public void addPro(String proName, int proNo) {
		OwnedProperty[proNo] = proNo;
		OwnedProName += (proName + "<br>");
	}
	
	/**
	 * Remove the property that the player sold out 
	 * @param proNo  number of the sold property 
	 */
	public void reducePro(int proNo) {
		OwnedProperty[proNo] = -1;
		// reset Owned Property Name list
		OwnedProName = "";
		for(int i=0; i<PublicData.numProperty; i++) {
			if(OwnedProperty[i] >= 0) {
				OwnedProName += ((String)PublicData.proInfo[i][PublicData.proIndex.NAME.ordinal()] + "<br>");
			}
		}
	}
	
	/**
	 * Player get a Jail-free card
	 */
	public void getJailFreeCard() {
		JailCard = JailCard + 1;
	}
	
	/**
	 * Return number of owned jail card
	 * @return jail card amount 
	 */
	public int viewJailCard() {
		return JailCard;
	}
	
	/**
	 * Return if the player is in jail 
	 * @return  true if in jail, false otherwise 
	 */
	public boolean getState() {
		return isPrision;
	}
	
	/**
	 * Change state of the player when in jail or set free 
	 */
	public void setState() {
		isPrision = !isPrision;
	}
	
	/**
	 * Add number of color owned by player, 
	 * If a color == 3 then the player owns a color group
	 * for energy company and airport, color == 2 then the 
	 * player owns the color group
	 * @param proNo  which property stored in public database 
	 */
	public void addColor(int proNo) {
		switch((String)PublicData.proInfo[proNo][PublicData.proIndex.COLOR.ordinal()]) {
		case "pink": 
			OwnedColor[0]++; 
			if(OwnedColor[0] == 3)  // if own all properties of a color group
				OwnedGroup[0] = "pink";
			break;  
		case "green": 
			OwnedColor[1]++; 
			if(OwnedColor[1] == 3)
				OwnedGroup[1] = "green";
			break; 
		case "blue": 
			OwnedColor[2]++; 
			if(OwnedColor[2] == 3)
				OwnedGroup[2] = "blue";
			break;  
		case "yellow": 
			OwnedColor[3]++; 
			if(OwnedColor[3] == 3)
				OwnedGroup[3] = "yellow";
			break;  
		case "gray": 
			OwnedColor[4]++; 
			if(OwnedColor[4] == 3)
				OwnedGroup[4] = "gray";
			break;  
		case "red": 
			OwnedColor[5]++; 
			if(OwnedColor[5] == 3)
				OwnedGroup[5] = "red";
			break; 
		case "violet": 
			OwnedColor[6]++; 
			if(OwnedColor[6] == 3)
				OwnedGroup[6] = "violet";
			break; 
		case "brown": 
			OwnedColor[7]++; 
			if(OwnedColor[7] == 3)
				OwnedGroup[7] = "brown";
			break; 
		case "airport": 
			OwnedColor[8]++; 
			if(OwnedColor[8] == 2)
				OwnedGroup[8] = "airport";
			break; 
		case "energy": 
			OwnedColor[9]++; 
			if(OwnedColor[9] == 2)
				OwnedGroup[9] = "energy";
			break; 
		}
	}
	
	/**
	 * Reduce number of color owned by player
	 * @param proNo  which property stored in public database 
	 */
	public void reduceColor(int proNo) {
		switch((String)PublicData.proInfo[proNo][PublicData.proIndex.COLOR.ordinal()]) {
		case "pink": OwnedColor[0]--; break;  
		case "green": OwnedColor[1]--; break; 
		case "blue": OwnedColor[2]--; break;  
		case "yellow": OwnedColor[3]--; break;  
		case "gray": OwnedColor[4]--; break;  
		case "red": OwnedColor[5]--; break;
		case "violet": OwnedColor[6]--; break;
		case "brown": OwnedColor[7]--; break;
		case "airport": OwnedColor[8]--; break;
		case "energy": OwnedColor[9]--; break;
		}
	}
}
