package common;

import static org.junit.Assert.*;

import org.junit.Test;

public class Properties_Test {

	@Test
	/**
	 * Test if returned rent is correct
	 * 2 houses build up on this property 
	 */
	public void test_findProNo() {
		Properties property = new Properties("TesterPro", 20, 20, 40,
		          							 60, 80, 100, 120,
		          							 100, 10, "pink", 2, 
		          							 0, "Tester");

		int res = property.getRent();
		assertEquals(60, res);
	}

}
