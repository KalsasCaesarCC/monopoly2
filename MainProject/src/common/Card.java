package common;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Represents fate & chance cards 
 * @author Ruibin Chen
 *
 */
public class Card {
	
	private String[] fateCards = new String[16];
	private String[] chanceCards = new String[16];
	private List <Integer> fateCardsList = new ArrayList<>();
	private List <Integer> chanceCardsList = new ArrayList<>();
	
	/**
	 * Create all cards 
	 */
	public Card() {
		fateCards[0] = "Advance to 'GO'. (Collect $200)";
		fateCards[1] = "Bank error in your favor. Collect $200"	;
		fateCards[2] = "Doctor's fees. {fee} Pay $50.";
		fateCards[3] = "From sale of stock you get $50.";
		fateCards[4] = "Get Out of Jail Free";
		fateCards[5] = "Go to Jail. Go directly to jail. Do not pass Go, Do not collect $200.";
		fateCards[6] = "Grand Opera Night.Collect $50 from every player for opening night seats.";
		fateCards[7] = "Holiday Fund matures. Receive $100.";
		fateCards[8] = "Income tax refund. Collect $20.";
		fateCards[9] = "It's your birthday.Collect $10 from every player";
		fateCards[10]= "Life insurance matures �C Collect $100 ";
		fateCards[11]= "Hospital Fees. Pay $50. ";
		fateCards[12]= "School Fees. Pay $50. ";
		fateCards[13]= "Receive $25 consultancy fee";
		fateCards[14]= "You are assessed for street repairs: Pay $40 per house and $115 per hotel you own.";
		fateCards[15]= "You have won second prize in a beauty contest. Collect $10. ";
		
		chanceCards[0] = "Advance to 'GO'. (Collect $200) ";
		chanceCards[1] = "Advance to 'Fineland', If you pass Go, collect $200.";
		chanceCards[2] = "Advance token to Energy Company. If unowned, you may buy it from the Bank."
				+ "If owned, throw dice and pay owner a total 10 times the amount thrown.";
		chanceCards[3] = "Advance token to the nearest Airplane and pay owner twice the rental to which he/she is otherwise entitled. "
				+ "If Airplane is unowned, you may buy it from the Bank. ";
		chanceCards[4] = "Bank pays you dividend of $50. ";
		chanceCards[5] = "Get out of Jail Free. ";
		chanceCards[6] = "Go to Jail. ";
		chanceCards[7] = "You inherit $100. ";
		chanceCards[8] = "Make general repairs on all your property: For each house pay $25, For each hotel pay $100.";
		chanceCards[9] = "Pay poor tax of $15";
		chanceCards[10]= "Take a trip to Beijing Airplane.If you pass Go, collect $200.";
		chanceCards[11]= "Take a walk on the Turkey.";
		chanceCards[12]= "You have been elected Chairman of the Board. Pay each player $50";
		chanceCards[13]= "Your building and loan matures. Receive $150. ";
		chanceCards[14]= "Go back to Greece";
		chanceCards[15]= "You have won a crossword competition. Collect $100.";
		
		shuffleCards();
	}
	
	/**
	 * At here, fate & chance cards will be shuffled
	 * Once a player get the first fate or chance card, 
	 * the card which was used will be but in the end of the sequence.
	 * Next time, player will get the second card from the sequence,and so on.
	 * Until get the last card, then a new rotation start.
	 */
	public void shuffleCards() {
		for (int i = 0; i < 16; i++) {
			chanceCardsList.add(i);
			fateCardsList.add(i);
		}
		Collections.shuffle(fateCardsList);
		Collections.shuffle(chanceCardsList);		
	}
	
	public int getFateCard() {
		fateCardsList.add(fateCardsList.get(0));
		return fateCardsList.remove(0);
	}
		
	public int getChanceCard() {
		chanceCardsList.add(chanceCardsList.get(0));
		return chanceCardsList.remove(0);	
	}
	
	/**
	 * Return the content of a Chance card
	 * @param cardNo  the sequence of card
	 * @return description of the card
	 */
	public String getChanceInfo(int cardNo) {
		return chanceCards[cardNo];
	}
	
	/**
	 * Return the content of a Fate card
	 * @param cardNo  the sequence of card
	 * @return description of the card
	 */
	public String getFateInfo(int cardNo) {
		return fateCards[cardNo];
	}
}

