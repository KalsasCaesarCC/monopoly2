package gamePanels;

import java.awt.Color;
import java.awt.Image;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

import common.PublicData;

/**
 * Panel of properties information 
 * @author Cheng Chen 
 *
 */
@SuppressWarnings("serial")
public class PropertyPanel extends JPanel {
	private JLabel proPanel, txtProSelec, txtProOwned, txtHouse, txtHotel;
	private JComboBox<String> proSelection;
	private ImageIcon imgProPanel;
	
	/**
	 * Creates property panel 
	 * @param x position in main frame 
	 * @param y position in main frame 
	 * @param width panel width 
	 * @param height panel width
	 */
	public PropertyPanel(int x, int y, int width, int height) {
		setBounds(x, y, width, height);
		setBorder(BorderFactory.createLineBorder(Color.black, 3));
		setBackground(new Color(169,209,142));
		setLayout(null);
		
		initComponents();
		
		add(proPanel);
		add(proSelection);
		add(txtProSelec);
		add(txtProOwned);
		add(txtHouse);
		add(txtHotel);
	}
	
	/**
	 * Initiate all components 
	 */
	private void initComponents() {
		proPanel = new JLabel();
		txtProSelec = new JLabel("Select Property:");
		txtProOwned = new JLabel("Property Owned by: Null");
		txtHouse = new JLabel("House Owned: 0");
		txtHotel = new JLabel("Hotel Owned: 0");
		proSelection = new JComboBox<String>();
		
		txtProSelec.setBounds(50, 20, 150, 30);
		txtProOwned.setBounds(50, 85, 200, 30);
		txtHouse.setBounds(50, 110, 200, 30);
		txtHotel.setBounds(50, 135, 200, 30);
		
		proSelection.setBounds(50, 50, 210, 30);
		for (int i = 0; i < PublicData.proInfo.length; i++ ) {
			proSelection.addItem((String) PublicData.proInfo[i][PublicData.proIndex.NAME.ordinal()]);
		}
		
		proPanel.setBounds(50, 160, 300, 525);
		setProInfo((String)PublicData.proInfo[0][PublicData.proIndex.NAME.ordinal()],
				   (int)PublicData.proInfo[0][PublicData.proIndex.NHOUSE.ordinal()],
				   (int)PublicData.proInfo[0][PublicData.proIndex.NHOTEL.ordinal()],
				   (String)PublicData.proInfo[0][PublicData.proIndex.OWNER.ordinal()]);
	}
	
	/**
	 * Return selected property from property selection ComboBox 
	 * @return  index of selected property 
	 */
	public int getSelectedPro() {
		return proSelection.getSelectedIndex();
	}
	
	/**
	 * Set/Update properties information 
	 * @param proName name of property 
	 * @param nHouse number of houses built 
	 * @param nHotel number of hotel built
	 * @param owner name of property owner 
	 */
	public void setProInfo(String proName, int nHouse, int nHotel, String owner) {
		imgProPanel = new ImageIcon(this.getClass().getClassLoader().getResource("images/Properties/" + proName + ".png"));
		imgProPanel.setImage(imgProPanel.getImage().getScaledInstance(proPanel.getWidth(), 
				proPanel.getHeight(), Image.SCALE_DEFAULT ));
		
		proPanel.setIcon(imgProPanel);
		
		txtProOwned.setText("Property Owned by: " + owner);
		txtProOwned.setForeground(owner == "Null" ? Color.black : Color.red);
		txtHouse.setText("House Owned: " + String.valueOf(nHouse));
		txtHotel.setText("Hotel Owned: " + String.valueOf(nHotel));
	}
}
