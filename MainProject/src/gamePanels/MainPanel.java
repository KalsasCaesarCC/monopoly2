package gamePanels;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Toolkit;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

import common.PublicData;

/**
 * Main chess board panel 
 * @author Cheng Chen
 *
 */
@SuppressWarnings("serial")
public class MainPanel extends JPanel {
	private JLabel mainPanel;
	private ImageIcon imgMainPanel;
	private int numOfPly;
	private int [] plyX;
	private int [] plyY;
	
	/**
	 * Creates chess board panel 
	 * @param x position in main frame 
	 * @param y position in main frame 
	 * @param width panel width 
	 * @param height panel width 
	 * @param nPly number of players
	 */
	public MainPanel(int x, int y, int width, int height, int nPly) {
		numOfPly = nPly;
		plyX = new int[nPly];
		plyY = new int[nPly];
		mainPanel = new JLabel();
		imgMainPanel = new ImageIcon(this.getClass().getClassLoader().getResource("images/mainPanel.png"));
		
		for(int i=0; i<nPly; i++) {
			plyX[i] = PublicData.plyInitX[i];
			plyY[i] = PublicData.plyInitY[i];
		}
		
		setBounds(x, y, width, height);
		setBorder(BorderFactory.createLineBorder(Color.black, 3));
		setLayout(null);
		setBackground(new Color(169,209,142));
	
		mainPanel.setBounds(0, 0, width, height);
		imgMainPanel.setImage(imgMainPanel.getImage().getScaledInstance(mainPanel.getWidth(), 
				mainPanel.getHeight(), Image.SCALE_DEFAULT ));
		mainPanel.setIcon(imgMainPanel);
		
		add(mainPanel);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		
		drawPly(g);
	}
	
	/**
	 * Draw players on main panel 
	 * @param g token images 
	 */
	public void drawPly(Graphics g) {
		int tokenSize = 40;	
		Image imagePly[] = new Image[numOfPly];
		String[] imgPath = {"images/Tokens/Magallan.png",
							"images/Tokens/Silver.png",
							"images/Tokens/Texas.png",
							"images/Tokens/Lapland.png",
							"images/Tokens/Warfarin.png",
							"images/Tokens/Kaltsit.png"};
		
		for (int i=0; i<numOfPly; i++) {
			imagePly[i] = Toolkit.getDefaultToolkit().getImage(this.getClass().getClassLoader().getResource(imgPath[i]));
			g.drawImage(imagePly[i], plyX[i], plyY[i], tokenSize, tokenSize, this);
		}
	}
	
	/**
	 * Calculate position of tokens and draw 
	 * @param blockNo block number
	 * @param plyNo player number 
	 */
	public void plyCalculation(int blockNo, int plyNo) {
		if(blockNo <= 10) {
			plyX[plyNo] = PublicData.plyInitX[plyNo] - (100 + 80*(blockNo-1));
			plyY[plyNo] = PublicData.plyInitY[plyNo];
		} else if (blockNo <= 20) {
			plyX[plyNo] = PublicData.plyInitX[plyNo] - 840;
			plyY[plyNo] = PublicData.plyInitY[plyNo] - (110 + 79*(blockNo-11));
		} else if (blockNo <= 30) {
			plyX[plyNo] = PublicData.plyInitX[plyNo] - (100 + 80*(29 - blockNo));
			plyY[plyNo] = PublicData.plyInitY[plyNo] - 842;
		} else {
			plyX[plyNo] = PublicData.plyInitX[plyNo];
			plyY[plyNo] = PublicData.plyInitY[plyNo] - (110 + 79*(39 - blockNo));
		}
		repaint();  // redraw the panel 
	}
}

