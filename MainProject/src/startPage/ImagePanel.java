package startPage;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

/**
 * ImagePanel 
 * @author Cheng Chen
 *
 */
@SuppressWarnings("serial")
public class ImagePanel extends JPanel {
	private Dimension d;
	private Image image;
	
	/**
	 * Creates an image panel 
	 * @param d image frame size 
	 * @param image source of image
	 */
	public ImagePanel(Dimension d, Image image) {
		super();
		
		this.setLayout(null);
		this.d = d;
		this.image = image;
	}
	
	/**
	 * Draw image to image panel 
	 */
	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.drawImage(image, 0, 0, d.width, d.height, this);
	}
}
